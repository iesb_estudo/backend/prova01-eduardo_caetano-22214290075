const mongoose = require('mongoose');

const schema = mongoose.Schema({
    tipoPessoa: {
        type: String,
        required: true,
        validate: {
            validator: function(vTipoPessoa) {
                return vTipoPessoa === "PF" || vTipoPessoa === "PJ";
            },
            message: props => `O tipo de pessoa deve ser "PF" (Pessoa Física) ou "PJ" (Pessoa Jurídica).`
        }
    },
    nome: {
        type: String,
        required: true,
        trim: true,
        minlength: 2,
        maxlength: 20 
    },
    salario: {
        type: Number,
        required: true,
        min: [1412, 'O salário não pode ser menor que o salário mínimo.']
    },
    cpf: {
        type: String,
        match: [/^(\d{3}\.?\d{3}\.?\d{3}-?\d{2})$/, 'Formato de CPF inválido. Use o formato 999.999.999-99'],
    },
    cnpj: {
        type: String,
        match: [/^(\d{2}\.?\d{3}\.?\d{3}\/?\d{4}-?\d{2})$/, 'Formato de CNPJ inválido. Use o formato 99.999.999/9999-99.']
    },
    sexo: {
        type: String,
        enum: ["M", "F"],
        validate: {
            validator: function(value) {
                return this.tipoPessoa !== 'PJ' || !value;
            },
            message: props => `O campo sexo só é permitido para Pessoa Física (PF).`
        }
    },
    cargo: {
        type: String,
        enum: ["Estagiario", "Tecnico", "Gerente", "Diretor", "Presidente"],
        validate: {
            validator: function(value) {
                return this.tipoPessoa !== 'PJ' || !value;
            },
            message: props => `O campo cargo só é permitido para Pessoa Física (PF).`
        }
    }
}, { timestamps: true });

schema.pre('validate', function(next) {
    if (this.tipoPessoa === 'PJ' && !this.cnpj) {
        const err = new Error('CNPJ não informado');
        next(err);
    } else if (this.tipoPessoa === 'PF' && !this.cpf) {
        const err = new Error('CPF não informado');
        next(err);
    }else if ((this.cpf && this.cnpj) || (!this.cpf && !this.cnpj)) {
        const err = new Error('Favor informar somente uma opção, CPF ou CNPJ, de acordo com o Tipo de Pessoa');
        next(err);
    } else {
        next();
    }
});

const Colaborador = mongoose.model('Colaborador', schema);

module.exports = Colaborador;
