const Colaborador = require("../models/colaborador")

const colaboradorConTroller = {

    getALL: async (req, res) => {
        res.json(await Colaborador.find())
    },

    getColaboradorCargo: async (req, res) => {
        try {
            const { cargo } = req.params;
            const colaboradorCargos = await Colaborador.find({cargo});

            res.json({ colaboradorCargos });

        } catch (erro) {
            res.status(400).json(erro.message)
        }
    },

    create: async (req, res) => {
        try {
            const novoColaborador = await Colaborador.create(req.body);
            res.status(201).json(novoColaborador);

        } catch (erro) {
            res.status(400).json(erro.message)
        }
    },

    aplicarReajuste: async (req, res) => {
        try {
            const { id } = req.params;
            const { percentual } = req.body;

            if (percentual < 0 || percentual > 100) {
                return res.status(400).json({ message: "O percentual de reajuste deve estar entre 0 e 100." });
            }

            const colaborador = await Colaborador.findById(id);

            if (!colaborador) {
                return res.status(404).json({ message: "Colaborador não encontrado." });
            }

            const novoSalario = colaborador.salario * (1 + percentual / 100);

            await Colaborador.findByIdAndUpdate(id, { salario: novoSalario }, { new: true });

            res.json({ novoSalario });
        } catch (error) {
            res.status(500).json({ message: "Ocorreu um erro ao aplicar o reajuste salarial." });
        }
    }

}
module.exports = colaboradorConTroller