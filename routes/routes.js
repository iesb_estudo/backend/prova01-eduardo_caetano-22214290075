const express = require('express')
const router = express.Router()

const colaboradorConTroller = require('../controllers/colaboradorConTroller')

router.get('/', function(req, res){
    res.json({})
})

router.get('/colaboradores', (req, res) => colaboradorConTroller.getALL(req, res))
router.get('/colaboradores/:cargo', (req, res) => colaboradorConTroller.getColaboradorCargo(req, res))
router.post('/colaborador', (req, res) => colaboradorConTroller.create(req, res))
router.put('/colaborador/:id/reajuste', (req, res) => colaboradorConTroller.aplicarReajuste(req, res));

module.exports = router